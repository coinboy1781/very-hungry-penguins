# Jeu des pingouins
Vous incarnez un pingouin qui ne sait pas nager, mais qui aime bien manger. Vous
êtes seul sur une banquise représentée par une grille hexagonale. Une case peut
y être recouverte de glace, sinon il n’y a que de l’eau. Sur chaque case de
glace, vous pouvez pêcher un poisson. Vous pouvez ensuite vous déplacer sur une
autre case en glissant en ligne droite selon l’un des six axes de la grille,
aussi loin que vous voulez mais en restant sur la glace — vous ne savez pas
nager ! La case que vous avez quittée fond à ce moment, et la glace y devient de
l’eau. L’histoire ne dit pas si à la fin le pingouin mort affamé ou noyé... En
tout cas, votre objectif est de déterminer le parcours qui vous permettra de
manger un maximum de poissons.

# Règles du jeu
TODO avec illustrations

# Fichiers de configuration
TODO

# Intelligences artificielles
TODO
